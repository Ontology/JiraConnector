﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnector.Models
{
    public class GetIssuesRequest
    {
        public string ProjectKey { get; set; }
        public string StartAt { get; set; }
        public string MaxResults { get; set; }

        public string RestUrl { get; set; }

        public string ComputedApiUrl
        {
            get
            {
                return $"{RestUrl}search?jql={ApiKeys.Project}=\"{ProjectKey}\"&{ApiKeys.StartAt}={StartAt}&{ApiKeys.MaxResults}={MaxResults}";
            }
        }
        public GetIssuesRequest(string restUrl, string projectKey, string startAt, string maxResults)
        {
            RestUrl = restUrl;
            ProjectKey = projectKey;
            StartAt = startAt;
            MaxResults = maxResults;
        }
    }
}

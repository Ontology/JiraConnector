﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnector.Models
{
    public class GetProjectsRequest
    {
        public bool AllProjects { get; set; }
        public string RestUrl { get; set; }

        public string ApiKey
        {
            get
            {
                return ApiKeys.Project;
            }
        }

        public string ComputedApiUrl
        {
            get
            {
                return $"{RestUrl}{ApiKey}";
            }
        }
    }
}

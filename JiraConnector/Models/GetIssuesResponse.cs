﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnector.Models
{
    public class GetIssuesResponse
    {
        public clsOntologyItem Result { get; set; }
        public string Expand { get; set; }
        public int StartAt { get; set; }
        public int MaxResult { get; set; }
        public int Total { get; set; }
        public List<Issue> Issues { get; set; }
    }
}

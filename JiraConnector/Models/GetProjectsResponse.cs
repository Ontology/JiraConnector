﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnector.Models
{
    public class GetProjectsResponse
    {
        public clsOntologyItem Result { get; set; }
        public List<JiraProject> JiraProjects { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace JiraConnector.Models
{
    public class JiraProject
    {
        public string Self { get; set; }
        public string Id { get; set; }
        public string Key { get; set; }
        public string Description { get; set; }
        public Lead Lead { get; set; }
        public IList<Component> Components { get; set; }
        public IList<IssueType> IssueTypes { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public string AssigneeType { get; set; }
        public IList<object> Versions { get; set; }
        public string Name { get; set; }
        public Roles Roles { get; set; }
        public AvatarUrls AvatarUrls { get; set; }
        public ProjectCategory ProjectCategory { get; set; }
        public bool Simplified { get; set; }
    }
}

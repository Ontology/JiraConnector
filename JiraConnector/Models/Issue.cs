﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnector.Models
{
    public class Issue
    {
        public string Expand { get; set; }
        public string Id { get; set; }
        public string Self { get; set; }
        public string Key { get; set; }
        public Fields Fields { get; set; }
    }

}

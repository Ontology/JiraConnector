﻿namespace JiraConnector.Models
{
    public class Votes
    {
        public string Self { get; set; }
        public int VotesValue { get; set; }
        public bool HasVoted { get; set; }
    }

    
}

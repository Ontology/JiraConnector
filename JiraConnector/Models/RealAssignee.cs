﻿namespace JiraConnector.Models
{
    public class RealAssignee
    {
        public string Self { get; set; }
        public string Key { get; set; }
        public string AccountId { get; set; }
        public string Name { get; set; }
        public AvatarUrls AvatarUrls { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
    }
}

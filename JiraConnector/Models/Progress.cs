﻿namespace JiraConnector.Models
{
    public class Progress
    {
        public int ProgressValue { get; set; }
        public int Total { get; set; }
    }

    
}

﻿namespace JiraConnector.Models
{
    public class Component
    {
        public string Self { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Lead Lead { get; set; }
        public string AssigneeType { get; set; }
        public Assignee Assignee { get; set; }
        public string RealAssigneeType { get; set; }
        public RealAssignee RealAssignee { get; set; }
        public bool IsAssigneeTypeValid { get; set; }
        public string Project { get; set; }
        public int ProjectId { get; set; }
    }
}

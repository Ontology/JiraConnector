﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraConnector.Models
{

    public class Lead
    {
        public string Self { get; set; }
        public string Key { get; set; }
        public string AccountId { get; set; }
        public string Name { get; set; }
        public AvatarUrls AvatarUrls { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
    }
}

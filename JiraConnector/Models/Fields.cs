﻿using System;
using System.Collections.Generic;

namespace JiraConnector.Models
{
    public class Fields
    {
        public IssueType Issuetype { get; set; }
        public object Timespent { get; set; }
        public JiraProject Project { get; set; }
        public IList<object> FixVersions { get; set; }
        public object Aggregatetimespent { get; set; }
        public object Resolution { get; set; }
        public object Resolutiondate { get; set; }
        public int Workratio { get; set; }
        public Watches Watches { get; set; }
        public DateTime? LastViewed { get; set; }
        public DateTime Created { get; set; }
        public object Customfield10020 { get; set; }
        public object Customfield10021 { get; set; }
        public object Customfield10022 { get; set; }
        public Priority Priority { get; set; }
        public object Customfield10023 { get; set; }
        public object Customfield10024 { get; set; }
        public IList<object> Labels { get; set; }
        public object Customfield10017 { get; set; }
        public object Customfield10018 { get; set; }
        public object Customfield10019 { get; set; }
        public object Aggregatetimeoriginalestimate { get; set; }
        public object Timeestimate { get; set; }
        public IList<object> Versions { get; set; }
        public IList<object> Issuelinks { get; set; }
        public object Assignee { get; set; }
        public DateTime Updated { get; set; }
        public Status Status { get; set; }
        public IList<object> Components { get; set; }
        public object Timeoriginalestimate { get; set; }
        public object Description { get; set; }
        public IList<object> Customfield10010 { get; set; }
        public string Customfield10011 { get; set; }
        public object Customfield10012 { get; set; }
        public object Customfield10013 { get; set; }
        public object Customfield10014 { get; set; }
        public object Security { get; set; }
        public object Customfield10008 { get; set; }
        public object Customfield10009 { get; set; }
        public object Aggregatetimeestimate { get; set; }
        public string Summary { get; set; }
        public Creator Creator { get; set; }
        public IList<object> Subtasks { get; set; }
        public Reporter Reporter { get; set; }
        public Aggregateprogress Aggregateprogress { get; set; }
        public string Customfield10000 { get; set; }
        public object Customfield10001 { get; set; }
        public object Customfield10002 { get; set; }
        public object Customfield10003 { get; set; }
        public object Customfield10004 { get; set; }
        public object Environment { get; set; }
        public object Duedate { get; set; }
        public Progress Progress { get; set; }
        public Votes Votes { get; set; }
    }

    
}

﻿using JiraConnector.Models;
using Newtonsoft.Json.Linq;
using OntologyClasses.DataClasses;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace JiraConnector
{
    public class JiraConnector
    {

        private RestHeaderItem authorizationItem;
        private clsLogStates logStates = new clsLogStates();
        public async Task<GetProjectsResponse> GetProjects(GetProjectsRequest request)
        {
            var taskResult = await Task.Run<GetProjectsResponse>(() =>
            {
                var projectList = new GetProjectsResponse
                {
                    Result = logStates.LogState_Success.Clone()
                };


                var restClient = new RestClient(request.ComputedApiUrl);
                restClient.AddHandler("application/json", new DynamicJsonDeserializer());
                var restRequest = new RestRequest(method: Method.GET);

                restRequest.AddHeader(authorizationItem.Key, authorizationItem.Value);
                IRestResponse response = restClient.Execute(restRequest);
                projectList.JiraProjects = Newtonsoft.Json.JsonConvert.DeserializeObject<List<JiraProject>>(response.Content);

                return projectList;
            });

            return taskResult;
        }

        public async Task<GetIssuesResponse> GetIssues(GetIssuesRequest request)
        {
            var taskResult = await Task.Run<GetIssuesResponse>(() =>
            {
                

                var restClient = new RestClient(request.ComputedApiUrl);
                restClient.AddHandler("application/json", new DynamicJsonDeserializer());
                var restRequest = new RestRequest(method: Method.GET);

                restRequest.AddHeader(authorizationItem.Key, authorizationItem.Value);
                IRestResponse response = restClient.Execute(restRequest);
                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<GetIssuesResponse>(response.Content);
                result.Result = logStates.LogState_Success.Clone();
                return result;
            });

            return taskResult;

        }

        public JiraConnector(string userName, string password)
        {
            var authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes(String.Format("{0}:{1}", "tassilok@gmx.de", "oJ_4A2w/9j$Mz}K")));
            authorizationItem = new RestHeaderItem
            {
                Key = "Authorization",
                Value = $"Basic { authorization }"
            };
        }
    }
}
